\documentclass[11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage{charter}
\usepackage[margin=1in]{geometry}

\usepackage{color}
\usepackage[hyphens]{url}
\usepackage[colorlinks=true, linkcolor=black, urlcolor=blue]{hyperref}
\urlstyle{same}

\usepackage{lastpage}
\usepackage{fancyhdr}
\pagestyle{fancyplain}
\renewcommand{\headrulewidth}{0pt}
\fancyfoot{}
\fancyhf{}
\rfoot{\textbf{Page \thepage~of \pageref{LastPage}}}
\lfoot{\textbf{Econ 484/673 Outline}}
\cfoot{\textbf{Winter 2021}}

\begin{document}
\begin{center} {\large \textbf{ECON 484/673: Econometric Methods for High-Dimensional Data, Winter 2021} } \end{center}
\noindent \textbf{Instructor:} Tom Parker \\
\noindent \textbf{Course material on} the
\href{https://arts.uwaterloo.ca/~tmparker/484/econ484.html}{course website} \\
\noindent \textbf{Office Hours:} Thursdays, 9am~-~10am \\
\noindent \textbf{Email:} \href{mailto:tmparker@uwaterloo.ca}{tmparker@uwaterloo.ca}

\begin{description} 

\item[Course Objectives] \hfill \\ In this course, you will learn about data
  analysis techniques that are especially useful for potentially complex
  models of economic behaviour in data-rich environments.  We will first
  discuss methods detecting what features are important for decisionmakers
  and how to examine the uncertainty in statistical estimators.  Then we will
  turn to the way that standard statistical tools are used to detect
  correlations in the observed data, and the way that these tools are used to
  examine the causal impact of policy on individuals.  Finally, we will
  discuss dimension reduction and nonparametric methods for measuring
  relationships in high-dimensional data.

\item[References] \hfill \\ This course will follow the basic outlines of the
  first reference below.  However, course material will also be drawn from the
    second reference, which is quite helpful if this is your first exposure to
    these methods.
  \begin{itemize}
    \item M. Taddy (2019). \emph{Business Data Science}, McGraw Hill.
    \item G. James, D. Witten, T. Hastie and R. Tibshirani (2014). \emph{An
      Introduction to Statistical Learning}, Springer.
  \end{itemize}
    The James et al. book is available freely from an accompanying
    \href{http://faculty.marshall.usc.edu/gareth-james/ISL/}{website}.

  The examples in the notes use R, a freely available statistical software
  package.  You are not required to choose any particular language for
  assignments, although I encourage you to learn R.  It is very useful,
  popular among social scientists and free to use.

\item[Topics] \hfill \\ The material will be sequenced as follows.

  \begin{enumerate}
    \item Inference, multiple testing and resampling
    \item Regression and regularization
    \item Classification 
    \item Methods for causal inference
    \item Factors and principal components
    \item Nonparametric methods for complex data
  \end{enumerate}

\item[Evaluation] \hfill \\ Your mark will be made up of the following parts,
  with relative weights in parentheses below.  Assignments
    will be completed in rotating assigned groups of about three people.
    Group assignments and a description of best practices for completing
    assignments will be distributed when the first assignment is posted on the
    course website.
\begin{enumerate}
  \item (80\%) Assignments posted over the course of the term, approximately
    one every two to three weeks.
  \item (20\%) Final assignment, which will include an oral component during
    the final exam period.
\end{enumerate}
\end{description}

%\newpage
%\vspace{2em}
\noindent \textbf{Other administrative details:} \small{

\begin{description}
\item[Academic Integrity] \hfill \\ Academic Integrity: In order to maintain a culture of academic integrity, members of the University of Waterloo are expected to promote honesty, trust, fairness, respect and responsibility. See the \href{https://uwaterloo.ca/academic-integrity/}{UWaterloo Academic Integrity} webpage and the Arts \href{https://uwaterloo.ca/arts/undergraduate/student-support/academic-standing-understanding-your-unofficial-transcript/ethical-behaviour}{Academic Integrity} webpage for more information.

  Discipline: A student is expected to know what constitutes academic integrity, to avoid committing academic offences, and to take responsibility for his/her actions. A student who is unsure whether an action constitutes an offence, or who needs help in learning how to avoid offences (e.g., plagiarism, cheating) or about “rules” for group work/collaboration should seek guidance from the course professor, academic advisor, or the Undergraduate Associate Dean. When misconduct has been found to have occurred, disciplinary penalties will be imposed under \href{https://uwaterloo.ca/secretariat/policies-procedures-guidelines/policy-71}{Policy 71 – Student Discipline}. For information on categories of offenses and types of penalties, students should refer to Policy 71 - Student Discipline. For typical penalties check \href{https://uwaterloo.ca/secretariat/guidelines/guidelines-assessment-penalties}{Guidelines for the Assessment of Penalties}. 

    Grievance: A student who believes that a decision affecting some aspect of his/her university life has been unfair or unreasonable may have grounds for initiating a grievance. Read \href{https://uwaterloo.ca/secretariat/policies-procedures-guidelines/policy-70}{Policy 70 - Student Petitions and Grievances}, Section 4. When in doubt, please be certain to contact the department’s administrative assistant who will provide further assistance. 

    Appeals: A decision made or penalty imposed under Policy 70 - Student Petitions and Grievances (other than a petition) or Policy 71 - Student Discipline may be appealed if there is a ground. A student who believes he/she has a ground for an appeal should refer to \href{https://uwaterloo.ca/secretariat/policies-procedures-guidelines/policy-72}{Policy 72 - Student Appeals}. 

  \item[Accommodation for Students with Disabilities] \hfill \\ Note for students with disabilities: \href{https://uwaterloo.ca/accessability-services/}{The AccessAbility Services office}, located on the first floor of the Needles Hall extension (1401), collaborates with all academic departments to arrange appropriate accommodations for students with disabilities without compromising the academic integrity of the curriculum. If you require academic accommodations to lessen the impact of your disability, please register with the AS office at the beginning of each academic term. 

\item[Mental Health Support] \hfill \\ All of us need a support system. The faculty and staff in Arts encourage students to seek out mental health supports if they are needed.
    \begin{description}
      \item[On Campus] \hfill
        \begin{itemize}
          \item Counselling Services: \url{counselling.services@uwaterloo.ca} / 519-888-4567 ext 32655
          \item	\href{https://feds.ca/feds-services/uw-mates}{MATES}: one-to-one peer support program offered by Federation of Students (FEDS) and Counselling Services
          \item	Health Services Emergency service: located across the creek form Student Life Centre
        \end{itemize}

      \item[Off campus, 24/7] \hfill
        \begin{itemize}
          \item	\href{https://good2talk.ca}{Good2Talk}: Free confidential help line for post-secondary students. Phone: 1-866-925-5454
          \item	Grand River Hospital: Emergency care for mental health crisis. Phone: 519-749-433 ext. 6880
          \item	\href{http://here247.ca}{Here 24/7}: Mental Health and Crisis Service Team. Phone: 1-844-437-3247
          \item	\href{http://ok2bme.ca}{OK2BME}: set of support services for lesbian, gay, bisexual, transgender or questioning teens in Waterloo. Phone: 519-884-0000 extension 213
        \end{itemize}
    \end{description}
    Full details can be found online at the Faculty of ARTS \href{https://uwaterloo.ca/arts/get-mental-health-support-when-you-need-it}{website}
    Download \href{https://uwaterloo.ca/arts/sites/ca.arts/files/uploads/files/counselling_services_overview_002.pdf}{UWaterloo and regional mental health resources (PDF)}
    Download the \href{https://uwaterloo.ca/watsafe/}{WatSafe} app to your phone to quickly access mental health support information

\item[Territorial Acknowledgement] \hfill \\ We acknowledge that we live and work on the traditional territory of the Attawandaron (Neutral), Anishinaabeg and Haudenosaunee peoples.  The University of Waterloo is situated on the Haldimand Tract, the land promised to the Six Nations that includes ten kilometers on each side of the Grand River.

\end{description}

}

\end{document}

