#!/bin/sh

set -ev

Rscript -e "rmarkdown::render('${1%.*}.Rmd')"
