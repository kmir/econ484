---
title: Logistic regression and robust standard errors
author: Tom Parker
date: \today
output:
  beamer_presentation:
    slide_level: 2
    includes:
      in_header: custom.tex
fontsize: 11pt
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(cache=TRUE, scipen = 2, digits = 4)
options(scipen = 2, digits = 4)
```

# Logistic regression

## Binary responses
- Logistic regression is used in machine learning to run a regression of
  **binary response** data on covariates.  That is, the response is either 0 or
  1, "yes" or "no", "success" or "failure".
- The expected $Y$, forgetting about $X$ for the moment, is
  \begin{equation*}
    \ex{Y} = 0 \times \prob{Y = 0} + 1 \times \prob{Y = 1} = \prob{Y = 1},
  \end{equation*}
  so a regression framework (targeting $\ex{Y|X}$) provides estimates of
  $\prob{Y = 1 | X}$.
- How should we allow covariates to inform $\prob{Y = 1 | X}$?

## Generalized linear models
- A **generalized linear model** is one in which
  \begin{equation*}
    \ex{Y | X} = f(X\tr \beta).
  \end{equation*}
  Here $X$ is a vector so $X\tr \beta$ represents a linear combination or inner
  product of covariates and coefficients contained in the vector $\beta$.
- A generalized linear model combines the *linear* part, $X\tr \beta$, with a
  (possibly) nonlinear function $f$.
  - For linear regression, $f(x) = x$ is the identity function.
- The function $f$ is assumed known (specified by you) and is called a **link
  function**.  The linear part, $x\tr \beta$, is sometimes called the **index**.
- For binary responses, the job of the link is to squash $x\tr \beta$ values
  between 0 and 1.

## Logit link
- The **logit link** function is the transformation
  \begin{equation*}
    z \mapsto \frac{e^z}{1 + e^z}.
  \end{equation*}
- **Logistic regression** uses the logit link to model
  \begin{equation*}
    \prob{Y = 1 | X = x} = \frac{e^{x\tr \beta}}{1 + e^{x\tr\beta}} =
    \frac{\text{exp} \{\beta_1 + \beta_2 x_2 + \ldots + \beta_p x_p\}}{1 +
    \text{exp} \{\beta_1 + \beta_2 x_2 + \ldots + \beta_p x_p\}}.
  \end{equation*}
- If you have run a *probit* regression before, it was the same idea but with
  the normal CDF as the link function.

## Why the logit link?
- The logit link is used to model the **odds** that an observation is zero or
  one.
- Let $p = \prob{Y = 1 | x}$ and call $x\tr\beta = \hat{y}$, and write
  \begin{align*}
    p &= \frac{e^{\hat{y}}}{1 + e^{\hat{y}}} \\
    \Leftrightarrow p + p e^{\hat{y}} &= e^{\hat{y}} \\
    \Leftrightarrow p &= e^{\hat{y}} - p e^{\hat{y}} \\
    \Leftrightarrow \frac{p}{1 - p} &= e^{\hat{y}} \\
    \Leftrightarrow \log \left( \frac{p}{1 - p} \right) &= \hat{y}.
  \end{align*}

## Interpreting coefficients
- Logistic regression is a linear model for the **log odds** that $Y = 1$.
- Moving $x_k$ to $x_k + 1$ results in an increase of $\log (p / 1 - p)$ by
  $\beta_k$ units.
- That means that if you just want to know about odds, the effect of $x_k$ is
  multiplicative: a one-unit increase in $x_k$ means that $p / (1 - p)$ is
  *multiplied* by $e^{\beta_k}$.

## Example in R
- The `spam` data set is a classic ML example.
- Someone named "George" worked at HP Labs in the 1990s.  He collected 4601 of
  the emails he received and labeled 1813 of them as *spam* (bad emails) and the
  remaining 2788 emails as *ham* (good emails).
- George recorded 57 features of each email besides the indicator for
  spam-osity.
- The larger ML goal is to construct a spam filter, but for now let's just run
  some regressions to find correlations between email features and whether they
  are spam or not.

## Look at some features
```{r spam_read}
email <- read.csv("../data/lecture_examples/spam.csv")
table(email$spam, email$word_george)
```
If George's name is in the email, it is almost never spam.

```{r tab_free}
table(email$spam, email$word_free)
```
The word "free" is pretty positively correlated with spam, although it's not
extreme.

## Run a regression
- Here is a logistic regression of all the features on `spam` and what we can
  learn:

```{r george_reg}
spammy <- glm(spam ~ ., data = email, family = binomial)
```

- R's `glm()` is meant to look just like `lm()`.  The `family` argument tells
  `glm()` that the response is binary, and the link is logit by default.
  - The family can be in quotes, like `family = "binomial"`.
  - For probit use `family = binomial(link = "probit")`.
- In situations with complex models, you shouldn't worry about the warning.  It
  means that some of the emails are estimated as 100% spam or ham.

## Looking at coefficients
```{r model_effects}
b <- coef(spammy)
exp(b["word_george"])
exp(b["word_free"])
```

- `word_george` raises the odds that an email is spam by multiplying the odds
  ratio by `r exp(b["word_george"])`, which is to say, it lowers the odds by a
  lot.
- On the other hand, `word_free` multiplies the odds that an email is spam by
  `r exp(b["word_free"])`.

## Predicting probabilities
- The `predict` function works for `glm` objects.
- If you predict using `newdata` then the output will be $\hat{y} =
  x\tr\hat{\beta}$ values.  You can use sample data to see what the value of
  $\hat{y}$ was for a specific observation.

```{r spam_pred1}
predict(spammy, newdata = email[c(1, 4000), ])
```

- To get the actual probabilities, not log odds ratio, use the option
  `type = "response"` in your command:

```{r spam_pred2}
predict(spammy, newdata = email[c(1, 4000), ],
        type = "response")
```

(Note: email #1 was marked spam, email #4000 was marked ham.)

## Linear regression is a "generalized" linear model
- As mentioned before, linear regression is a GLM just using the link function
  $f(x) = x$.
- As such, `glm()` in R will also run regressions for you.  Before, for example,
  I ran `lm(log(sales) ~ log(price) + brand, data = oj)` to estimate an OJ
  regression.

\scriptsize
```{r oj_glm}
oj <- read.csv("../data/lecture_examples/oj.csv", stringsAsFactors = TRUE)
oj_lm <- lm(log(sales) ~ log(price) + brand, data = oj)
oj_glm <- glm(log(sales) ~ log(price) + brand, data = oj)
coef(oj_lm)
coef(oj_glm)
```
\normalsize

- The default `family` for `glm()` is `family = gaussian`, which is like `lm()`.

# Likelihood

## Likelihood and regression models
- If you look at `summary(spammy)` or `summary(oj_glm)` in R, you will see a lot
  of information that looks like regression summary information.
- However, some of it may be a little mysterious.
- GLMs are estimated using **maximum likelihood** estimation.
- We also see **deviance** mentioned in the summary output.
  1. **Likelihood** is a measure of how "likely" a parameter vector is for a
     parametric model, given observed data.
  2. **Deviance** is a measure of the lack of fit of a parametric model and
     observations.
- These amounts are linked:
  \begin{equation*}
    \text{deviance} = -2 \log \left( \text{likelihood} \right) + C,
  \end{equation*}
  where $C$ is a constant.  $C$ represents the likelihood that you would get if
  you had a *saturated* model, which means one parameter $\theta_i$ for each
  observation $y_i$.

## Likelihood and deviance for the linear model
- Deviance generalizes the sum of squared errors idea.
- Let's test what that means.  Assume that
  \begin{equation*}
    Y \sim \calN(X\tr \theta, \sigma^2),
  \end{equation*}
  where we assume that we know $\sigma^2$ for simplicity.
  - Likelihood needs a parametric model, so we need to assume one for $Y|X$ here.
- This means that the density for $Y_i | X_i$ is
  \begin{equation*}
    f(y_i, \theta | x_i) = \frac{1}{\sqrt{2\pi \sigma^2}} \exp \left\{ - \frac{(y_i -
    x_i\tr \theta)^2}{2 \sigma^2} \right\}.
  \end{equation*}
- *This is the likelihood for one observation.*  The likelihood given the whole
  sample is the product of all the different $f(y_i | x_i)$.

## Likelihood work
- Letting $\like$ be the likelihood function and $\ell$ be the
  loglikelihood function ($\ell = \log \like$),
  \begin{align*}
    \like(\theta) &= \prod_{i=1}^n f(y_i, \theta | x_i) \\
    {} &= \prod_{i=1}^n \frac{1}{\sqrt{2\pi \sigma^2}} \exp \left\{ - \frac{(y_i -
    x_i\tr \theta)^2}{2 \sigma^2} \right\} \\
    {} &= (2\pi \sigma^2)^{-n/2} \exp \left\{ -\frac{1}{2\sigma^2} \sum_{i=1}^n
    (y_i - x_i\tr \theta)^2 \right\} \\
    {} &\propto - \sum_{i=1}^n (y_i - x_i\tr \theta)^2.
  \end{align*}
- The deviance is **proportional to** minimizing the SSE (this is like adding
  $C$ in the general formula from before).

## Deviance in linear regression
- To summarize,
  \begin{equation*}
    \dev(\theta) \propto \sum_{i=1}^n (y_i - x_i\tr \theta)^2.
  \end{equation*}
- Minimizing the sum of squared errors is the same as minimizing the deviance of
  the model.
- Therefore the OLS estimator is a deviance-minimizing estimator (and a
  likelihood-maximizing one too), assuming the errors are normal.

## Logistic regression likelihood
- For the binomial model, first we perform a trick: letting $p_i = \prob{Y_i = 1
  | X_i}$, rewrite
  \begin{equation*}
    f(y_i, \theta | x_i) = \begin{cases} 1 &\text{with prob. } p_i \\ 0 &
    \text{with prob. } 1 - p_i \end{cases} = p_i^{y_i} (1 - p_i)^{1 - y_i}.
  \end{equation*}
- Now modeling $p_i$ with the linear index and logistic link, we get
  \begin{align*}
    \like(\theta) &= \prod_{i=1}^n p_i^{y_i} (1 - p_i)^{1 - y_i} \\
    {} &\stackrel{model}{=} \prod_{i=1}^n \left( \frac{\exp \{ x_i\tr \theta
    \}}{1 + \exp \{ x_i\tr \theta \}} \right)^{y_i} \left( \frac{1}{1 + \exp \{
    x_i\tr \theta \}} \right)^{1 - y_i}.
  \end{align*}

## Logistic regression deviance
- The deviance here isn't as pretty as in linear regression.
- Find the loglikelihood and multiply it by $-2$, and you get
  \begin{equation*}
    -2 \ell(\theta) = -2 \sum_{i=1}^n \left( y_i \log p_i + (1 - y_i) \log (1 -
    p_i) \right),
  \end{equation*}
  using the $p_i$ from last slide.
- The best you can do here is get everything over the same denominator ($1 +
  \exp\{x_i\tr \theta\}$ for each $i$) and remove it:
  \begin{equation*}
    \dev(\theta) \propto \sum_{i=1}^n \left( \log \left( 1 + \exp \left\{ x_i\tr
    \theta \right\} \right) - y_i x_i\tr \theta \right).
  \end{equation*}
  - Not as nice, but it's what your computer minimizes.

## Finding the deviance in R
- Here's the deviance from the OJ regression.

\scriptsize
```{r spam_deviance}
oj_glm
```
\normalsize

## Null and residual deviance and $R^2$
- There are *two* deviances.  What are they?
  1. The **residual deviance** is what I've been talking about so far.  Call
     this $D(\hat{\theta})$.
  1. The **null deviance** is what you would get if you just used an intercept.
    - For linear regression, call $D_0 = \sum_i (y_i - \bar{y})^2$.
    - For logistic, $D_0 = -2 \sum_i (y_i \log \bar{y} + (1 - y_i) \log (1 -
      \bar{y}))$.
- The difference between $D$ and $D_0$ is a measure of how much information the
  covariates give you.
- $R^2$ is the proportion of the deviance "explained" by $X$:
  \begin{equation*}
    R^2 = \frac{D_0 - D}{D_0} = 1 - \frac{D}{D_0}.
  \end{equation*}
- For linear regression (only!), $R^2$ is exactly what you know: $D_0 = SST$ and
  $D = SSE$.

# Non-standard standard errors

## Homoskedasticity and heteroskedasticity
- The previous linear model assumed homoskedasticity in the data (the logistic
  model automatically introduces some heteroskedasticity).
- In linear models there are a number of tools to more honestly report the
  standard errors of your coefficient estimates.
- Bootstrap methods can be used, but often they are not as accurate as simply
  estimating the way that heteroskedasticity affects $\hat{\theta}$.

## Standard errors for regression coefficients
- Suppose that 
  \begin{equation*}
    y_i = \alpha + \beta x_i + u_i,
  \end{equation*}
  where $\var{u_i | x_i} = \sigma_i^2$.  That is, we have a different $\sigma^2$
  for each observation!
- There is no way to estimate all those variances well as individual parameters,
  but we can still estimate the way that they average out to give us the
  standard error of $\hat{\beta}$.
- Recall that the standard deviation of the distribution of an *estimator*
  (instead of just any random variable) is called the **standard error** of the
  estimator.

## Variances
- First, we can write
  \begin{equation*}
    \hat{\beta} = \beta + \frac{\frac{1}{n} \sum_{i=1}^n (x_i -
    \bar{x})u_i}{\frac{1}{n} \sum_{i=1}^n (x_i - \bar{x})^2}.
  \end{equation*}
- As a result, under homoskedasticity, it can be shown that the variance of
  $\hat{\beta}$ is approximately
  \begin{equation*}
    \var{\hat{\beta}} \approx \frac{1}{n} \frac{\sigma^2}{\var{X}},
  \end{equation*}
  where $\sigma^2$ is the common variance of all the $U_i$.
- Under heteroskedasticity, it can be shown similarly that
  \begin{equation*}
    \var{\hat{\beta}} \approx \frac{1}{n} \frac{\ex{((X - \mu_X)U)^2}}{(\var{X})^2}.
  \end{equation*}

## HC variance estimation
- The last expression on the previous slide is difficult.  However, it can be
  estimated.
- For example $\var{X}$ is estimated with $\frac{1}{n} \sum_i (x_i -
  \bar{x})^2$.
- But how do you estimate $\var{(X - \mu)U}$?
- The basic solution is to estimate this with
  \begin{equation*}
    \frac{1}{n} \sum_{i=1}^n (x_i - \bar{x})^2 \hat{u}_i^2,
  \end{equation*}
  where $\hat{u}_i$ are the residuals from the regression.
- This is called a **heteroskedasticity-consistent** (HC) estimate of the
  variance of $\hat{\beta}$.  The standard error of $\hat{\beta}$ is just its
  square root.
  - Also called **robust** standard errors for short.

## HC standard errors in practice
- HC standard errors are very common in practice, and you should use them when
  you can.
- When $\beta$ is a vector, the expression from the last page looks like (using
  the $n \times p$ matrix $X$ to denote the data and $\Omega$ to be the $n
  \times n$ matrix of variances of the $u_i$)
  \begin{equation*}
    (X\tr X)^{-1} (X\tr \Omega X) (X\tr X)^{-1},
  \end{equation*}
  which is called a **sandwich matrix**.
- The R package `sandwich` is made for estimating sandwich matrices.
- There are similar methods for when errors are *clustered* (like observations
  come from one household).

## R example (homoskedasticity)
- We can try `sandwich` on one of our regressions.  First, a reminder of the
  not-robust standard errors:

\scriptsize
```{r oj}
oj <- read.csv("../data/lecture_examples/oj.csv", stringsAsFactors = TRUE)
reg <- lm(log(sales) ~ log(price) + brand, data = oj)
summary(reg)$coef
```
\normalsize

## R example (heteroskedasticity)
- Now for HC standard errors.  Here is a comparison.

\scriptsize
```{r robust_se}
library(sandwich)
compare <- matrix(0, 3, length(coef(reg)))
compare[1:2, ] <- t(summary(reg)$coef[, 1:2])
HC <- vcovHC(reg)
compare[3, ] <- sqrt(diag(HC))
dimnames(compare) <- list(c("coefficients", "old-fashioned", "het. robust"),
                          names(coef(reg)))
compare
```
\normalsize

- This one is pretty modest: the robust standard errors are almost always
  larger than the old-fashioned ones.
